$(function(){
    initNewsData();
});

function initNewsData() {
    var current_news = getCurrentNews();
    var title = current_news.fake ? 'FAKE: "'+current_news.title+'"' : 'FATO: "'+current_news.title+'"';
    var title_class = current_news.fake ? 'text-danger' : 'text-success';

    $('#news_title').addClass(title_class).text(title);
    $('#news_image').attr('src', 'img/'+current_news.image);
    $('#news_body').append(current_news.text);
}

function getCurrentNews() {
    var searchParams = new URLSearchParams(window.location.search)
    var news_id = searchParams.get('news');

    var current_news = {};

    for (var item of news) {
        if (item.id == news_id) {
            current_news = item;
            break;      
        }
    }
    
    return current_news;
}