$(function(){
    showCategoryNews();

});

function showCategoryNews() {
    var news_div = $('#category_news');

    var category = getCategory();
    var category_title = category == 'fake' ? 'FAKE' : 'FATO';
    var category_class = category == 'fake' ? 'text-danger' : 'text-success';

    $('#category_title').addClass(category_class).text(category_title);

    var index = 1;
    for (var item of news) {
        if (category == 'fake' && !item.fake) {
            continue;
        }

        if (category != 'fake' && item.fake) {
            continue;
        }

        title = item.fake ? 'FAKE: "'+item.title+'"' : 'FATO: "'+item.title+'"';
        title_class = item.fake ? 'text-danger' : 'text-success';

        var card = $('#card-template').clone().prop('id', 'card_'+index).show();
        card.find('.card-img').attr('src', 'img/'+item.image);
        card.find('.card-title').addClass(title_class).text(title);
        card.find('.card-caption').text(item.caption);
        card.attr('href', 'news-'+item.layout+'.html?news='+item.id);

        news_div.append(card);
        index++;
    }

}

function getCategory() {
    var searchParams = new URLSearchParams(window.location.search)
    return searchParams.get('category');
}