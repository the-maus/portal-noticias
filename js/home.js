$(function(){
    showHomeNews();

});

function showHomeNews() {
    var news_div = $('#home_news');

    var index = 1;
    for (var item of news) {
        title = item.fake ? 'FAKE: "'+item.title+'"' : 'FATO: "'+item.title+'"';
        title_class = item.fake ? 'text-danger' : 'text-success';

        var card = $('#card-template').clone().prop('id', 'card_'+index).show();
        card.find('.card-img-top').attr('src', 'img/'+item.image);
        card.find('.card-title').addClass(title_class).text(title);
        card.attr('href', 'news-'+item.layout+'.html?news='+item.id);

        news_div.append(card);
        index++;
    }

}