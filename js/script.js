$(function(){
    fixMenuSize();

    var offCanvas = document.getElementById('offcanvasNavbar');
    offCanvas.addEventListener('shown.bs.offcanvas', function () {
        fixMenuSize();    
    });
    offCanvas.addEventListener('hidden.bs.offcanvas', function () {
        fixMenuSize();    
    });
});

function fixMenuSize() {
    $('.menu').width($('.menu').parent().width() - ($('.logo').width() + 20));
}